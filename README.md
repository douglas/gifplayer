# Gifplayer

Shows gifs on the flow3r badge

## Install

Download repo and copy over to `apps` 

You can generate frames from your own gif using ffmpeg
```
ffmpeg -i rick.gif -vf scale=200:200 frames/frame_%d.png
```

## Warning
`ctx.image()` does not currently work in the simulator only on badge

If you load frames from the sd card they will likely play slower.
