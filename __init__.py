import st3m.run, random, leds

from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context

class gifplayer(Application):
    def __init__(self, context):
        super().__init__(context)
        self.current_frame = 1
        self.max_frames = 17

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        leds.set_all_rgb(0, 0, 255)
        leds.update()
        
    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        # Loop if we printed the last frame
        if self.current_frame > self.max_frames:
                self.current_frame = 1
        # Faster
        theframe = "/flash/sys/apps/gifplayer/frames/frame_"+str(self.current_frame)+".png"

        # Slower
        #theframe = "/sd/frames/frame_"+str(self.current_frame)+".png"

        # Draw frame
        ctx.image(theframe,-100,-100,200,200)

        # Increase frame
        self.current_frame += 1

if __name__ == "__main__":
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(gifplayer(ApplicationContext()))
